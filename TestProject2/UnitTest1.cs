using System;
using System.Collections;
using System.Linq;
using NUnit.Framework;
using Parser;
using Parser.Ast;
using Parser.Ast.Visitors;

namespace TestProject2
{
    public class Tests
    {

        [TestCase("2x - 3x + y= 0", "-x + y = 0")]
        [TestCase("x^2 + 3.5xy + y - y^2 + xy - y = 0", "x^2 + 4.5xy - y^2 = 0")]
        [Test]
        public void MergeShould(string input, string assertion)
        {
            var tokens = new Tokenizer().Tokenize(input).ToArray();
            
            AstExpression ast = new AstBuilder(tokens).ParseEquation();
            
            var monomialMergeVisitor = new MonomialMergeVisitor();
            
            ast = ast.Accept(monomialMergeVisitor);
            
            Assert.AreEqual(assertion, ast.ToString());
        }
        
        [TestCase("2x = 3x", "2x - 3x = 0")]
        [TestCase("2x + y= 3x - y", "2x + y - 3x + y = 0")]
        [Test]
        public void AllToLeftShould(string input, string assertion)
        {
            var tokens = new Tokenizer().Tokenize(input).ToArray();
            
            AstExpression ast = new AstBuilder(tokens).ParseEquation();

            var allMonomialsToLeftVisitor =  new MoveAllMonomialsToLeftVisitor();
            ast = ast
                .Accept(allMonomialsToLeftVisitor);
            
            Assert.AreEqual(assertion, ast.ToString());
        }
    }
}