﻿using System.Collections.Generic;
using System.Text;

namespace Parser
{
    public abstract class Token
    {
        
    }

    public class AddToken : Token
    {
        
    }

    public class Multiply : Token
    {
        
    }

    public class Pow : Token
    {

    }

    public class VariableToken : Token
    {
        public char Name { get; set; }
    }
    
    public class NumberToken : Token
    {
        public decimal Number { get; }

        public NumberToken(decimal number)
        {
            Number = number;
        }
    }

    public class MinusToken : Token
    {
        
    }

    public class Equality : Token
    {
        
    }
    
    public class Tokenizer
    {
        
        public IEnumerable<Token> Tokenize(string input)
        {
            using var iterator = input.GetEnumerator();
            
            bool finished = false;
            iterator.MoveNext();
            while (true)
            {
                if (finished)
                {
                    break;
                }
                
                var token = iterator.Current;
                
                switch (token)
                {
                    case '+':
                    {
                        if (!iterator.MoveNext())
                        {
                            finished = true;
                        }
                        yield return new AddToken();
                        break;
                    }
                    
                    case '*':
                    {
                        if (!iterator.MoveNext())
                        {
                            finished = true;
                        }
                        yield return new Multiply();
                        break;
                    }

                    case '^':
                    {
                        if (!iterator.MoveNext())
                        {
                            finished = true;
                        }
                        yield return new Pow();
                        break;
                    }

                    case '=':
                    {
                        if (!iterator.MoveNext())
                        {
                            finished = true;
                        }
                        yield return new Equality();
                        break;
                    }
                    
                    case '-':
                    {
                        if (!iterator.MoveNext())
                        {
                            finished = true;
                        }
                        yield return new MinusToken();
                        break;
                    }
                    
                    case ' ':
                    {
                        if (!iterator.MoveNext())
                        {
                            finished = true;
                        }
                        continue;
                    }
                }

                if (char.IsLetter(token))
                {
                    if (!iterator.MoveNext())
                    {
                        finished = true;
                    }

                    yield return new VariableToken()
                    {
                        Name = token
                    };
                    continue;
                }

                if (char.IsDigit(token))
                {
                    var sb = new StringBuilder();

                    while (char.IsDigit(token) || token == '.')
                    {
                        sb.Append(token);
                        if (!iterator.MoveNext())
                        {
                            finished = true;
                            break;
                        }
                        else
                        {

                            token = iterator.Current;
                        }
                    }

                    yield return new NumberToken(decimal.Parse(sb.ToString()));
                }
            }
        }
    }
}