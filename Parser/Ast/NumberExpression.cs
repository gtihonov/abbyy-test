﻿using System.Globalization;
using Parser.Ast.Visitors;

namespace Parser.Ast
{
    public class NumberExpression : AstExpression
    {
        public decimal Value { get; set; }
        
        public NumberExpression(decimal value)
        {
            Value = value;
        }
        
        public override string ToString()
        {
            return Value.ToString(CultureInfo.InvariantCulture);
        }

        public override AstExpression Accept(IAstTransformationVisitor visitor)
        {
            return visitor.VisitNumberExpression(this);
        }

    }
}