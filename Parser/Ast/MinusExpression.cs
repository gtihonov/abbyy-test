﻿using Parser.Ast.Visitors;

namespace Parser.Ast
{
    public class MinusExpression : AstExpression
    {
        public override AstExpression Accept(IAstTransformationVisitor visitor)
        {
            return visitor.VisitMinusExpression(this);
        }
    }
}