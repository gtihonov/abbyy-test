﻿using Parser.Ast.Visitors;

namespace Parser.Ast
{
    public class AddExpression : BinaryExpression
    {

        public override AstExpression Accept(IAstTransformationVisitor visitor)
        {
            return visitor.VisitAddExpression(this);
        }
        
        public override string ToString()
        {
            var right = Right.ToString();

            if (right.StartsWith('-'))
            {
                return $"{Left.ToString()} - {right.Substring(1)}";  
            }
            return $"{Left.ToString()} + {Right.ToString()}";
        }

    }
}