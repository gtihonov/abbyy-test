﻿using System.Collections.Generic;
using System.Linq;

namespace Parser.Ast
{
    public class AstBuilder
    {
        private readonly Token[] _tokens;
        private int _current = -1;
        
        public AstBuilder(IEnumerable<Token> tokens)
        {
            _tokens = tokens.ToArray();
        }

        public EqualionExpression ParseEquation()
        {
            var left = ParsePolynomial();
            var token = NextToken();

            switch (token)
            {
                case Equality _:
                {
                    var expr = new EqualionExpression()
                    {
                        Left = left,
                        Right = ParsePolynomial()
                    };

                    expr.Left.Parent = expr;
                    expr.Right.Parent = expr;

                    return expr;
                }
                
                default: return null;
            }
        }

        public AstExpression ParsePolynomial()
        {
            var left = ParseMonomial();

            if (left is PowExpression)
            {
                var replace  = new MultiplyExpression()
                {
                    Left = new NumberExpression(1),
                    Right = left
                };

                replace.Left.Parent = replace;
                replace.Right.Parent = replace;

                left = replace;
            }
            
            if (left == null)
            {
                return null;
            }
            
            var right = ParsePolynomial();

            if (right != null)
            {
                var expr = new AddExpression()
                {
                    Left = left,
                    Right = right
                };

                expr.Left.Parent = expr;
                expr.Right.Parent = expr;

                return expr;
            }
            
            return left;
        }

        public AstExpression ParseMonomial()
        {
            var primary = ParsePrimary();

            if (primary == null)
            {
                return null;
            }
            
            var token = NextToken();

            switch (token)
            {
                case NumberToken _:
                {
                    Rollback();

                    MultiplyExpression expr;
                    if (primary is NumberExpression privateNum)
                    {
                        var nextPrimary = ParsePrimary() as NumberExpression;
                        privateNum.Value *= nextPrimary.Value;
                    }
                    
                    
                    expr = new MultiplyExpression()
                    {
                        Left = primary,
                        Right = ParseMonomial()
                    };

                    expr.Left.Parent = expr;
                    expr.Right.Parent = expr;

                    return expr;
                }
                case VariableToken _:
                {
                    Rollback();
                    
                    var expr =  new MultiplyExpression()
                    {
                        Left = primary,
                        Right = ParseMonomial()
                    };

                    expr.Left.Parent = expr;
                    expr.Right.Parent = expr;

                    return expr;
                }
                case Multiply _:
                {
                    var expr = new MultiplyExpression()
                    {
                        Left = primary,
                        Right = ParsePrimary()
                    };

                    expr.Left.Parent = expr;
                    expr.Right.Parent = expr;

                    return expr;
                }
                case Pow _:
                {
                    var expr = new PowExpression()
                    {
                        Left = primary,
                        Right = ParsePrimary()
                    };

                    expr.Left.Parent = expr;
                    expr.Right.Parent = expr;

                    return expr;
                }
                default:
                {
                    Rollback();
                    return primary;
                }
            }
        }

        private AstExpression ParsePrimary()
        {
            var token = NextToken();

            switch (token)
            {
                case NumberToken numberToken:
                    return new NumberExpression(numberToken.Number);
                case VariableToken variableToken:
                    return new VariableExpression(variableToken.Name);
                case MinusToken minusToken:
                    return new NumberExpression(-1);
                case AddToken addToken:
                    return new NumberExpression(1);
                default:
                {
                    Rollback();
                    return null;
                }
            }
        }

        private Token NextToken()
        {
            _current++;
            if (_current == _tokens.Length)
            {
                return null;
            }

            return _tokens[_current];
        }

        private void Rollback()
        {
            _current--;
        }
    }
}