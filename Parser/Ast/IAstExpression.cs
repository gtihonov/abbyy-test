﻿using Parser.Ast.Visitors;

namespace Parser.Ast
{
    public interface IAstExpression
    {
        public AstExpression Accept(IAstTransformationVisitor visitor);
        
        public AstExpression Parent { get; set; }
    }
}