﻿using Parser.Ast.Visitors;

namespace Parser.Ast
{
    public class EqualionExpression : BinaryExpression
    {
        public override string ToString()
        {
            return $"{Left} = {Right}";
        }
        
        public override AstExpression Accept(IAstTransformationVisitor visitor)
        {
            return visitor.VisitEqualityExpression(this);
        }
    }
}