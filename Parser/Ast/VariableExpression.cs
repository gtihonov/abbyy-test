﻿using Parser.Ast.Visitors;

namespace Parser.Ast
{
    public class VariableExpression : AstExpression
    {
        public char Name;


        public VariableExpression(char name)
        {
            Name = name;
        }

        public override string ToString()
        {
            return new string(new []{Name});
        }

        public override AstExpression Accept(IAstTransformationVisitor visitor)
        {
            return  visitor.VisitVariableExpression(this);
        }
    }
}