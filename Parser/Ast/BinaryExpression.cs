﻿namespace Parser.Ast
{
    public abstract class BinaryExpression : AstExpression
    {
        public AstExpression Left { get; set; }
        
        public AstExpression Right { get; set; }
    }
}