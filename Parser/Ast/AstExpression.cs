﻿using System.Diagnostics;
using Parser.Ast.Visitors;

namespace Parser.Ast
{
    [DebuggerDisplay("{DebugView}")]
    public abstract class AstExpression : IAstExpression
    {
        public abstract AstExpression Accept(IAstTransformationVisitor visitor);
        
        public AstExpression Parent { get; set; }

        private string DebugView => this.ToString();
        public bool IsChildOf(AstExpression expression)
        {
            if (Parent == expression)
            {
                return true;
            }

            return Parent != null && Parent.IsChildOf(expression);
        }
    }
}