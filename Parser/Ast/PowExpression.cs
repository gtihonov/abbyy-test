﻿using Parser.Ast.Visitors;

namespace Parser.Ast
{
    public class PowExpression : BinaryExpression
    {
        public override string ToString()
        {
            return $"{Left}^{Right}";
        }
        
        public override AstExpression Accept(IAstTransformationVisitor visitor)
        {
            return visitor.VisitPowExpression(this);
        }
        
    }
}