﻿using Parser.Ast.Visitors;

namespace Parser.Ast
{
    public class MultiplyExpression : BinaryExpression
    {
        public override string ToString()
        {
            if (Left.ToString() == "1")
            {
                return Right.ToString();
            }
            
            if (Left.ToString() == "-1")
            {
                return $"-{Right.ToString()}";
            }
            return $"{Left}{Right}";
        }
        
        public override AstExpression Accept(IAstTransformationVisitor visitor)
        {
            return visitor.VisitMultiplyExpression(this);
        }
        
    }
}