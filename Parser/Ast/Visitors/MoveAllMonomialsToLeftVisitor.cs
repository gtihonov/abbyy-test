﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Parser.Ast.Visitors
{
    public class MoveAllMonomialsToLeftVisitor : AstTransformationVisitorBase
    {
        private Stack<AstExpression> _expressions = new Stack<AstExpression>();

        private Queue<AstExpression> _queueExpressions = new Queue<AstExpression>();
        public override AstExpression VisitEqualityExpression(EqualionExpression expression)
        {
            expression.Left = expression.Left.Accept(this);
            var leftRoot = _expressions.Pop();

            _expressions.Clear();
            _queueExpressions.Clear();
            expression.Right = expression.Right.Accept(this);

            while (_queueExpressions.TryDequeue(out var rightSideExpression))
            {
                ((NumberExpression) ((MultiplyExpression) rightSideExpression).Left).Value *= -1;
                if (leftRoot is BinaryExpression leftBinaryExpression)
                {
                    var newAdd = new AddExpression()
                    {
                        Left = leftBinaryExpression.Left,
                        Right = rightSideExpression,
                        Parent = leftBinaryExpression
                    };
                    leftBinaryExpression.Left = newAdd;
                    rightSideExpression.Parent = newAdd;
                    leftRoot = newAdd;
                }
            }

            expression.Right = new NumberExpression(0)
            {
                Parent = expression
            };
            
            return expression;
        }

        public override AstExpression VisitAddExpression(AddExpression expression)
        {
            _expressions.Push(expression);
            return base.VisitAddExpression(expression);
        }

        public override AstExpression VisitMinusExpression(MinusExpression expression)
        {
            _expressions.Push(expression);
            return base.VisitMinusExpression(expression);
        }

        public override AstExpression VisitMultiplyExpression(MultiplyExpression expression)
        {
            if (expression.Parent is EqualionExpression)
            {
                _expressions.Push(expression.Parent); 
            }
            
            if (expression.Parent is EqualionExpression || expression.Parent is AddExpression)
            {
                _queueExpressions.Enqueue(expression);
            }
           
            return base.VisitMultiplyExpression(expression);
        }
    }
}