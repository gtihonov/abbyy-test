﻿using System.Collections.Generic;

namespace Parser.Ast.Visitors
{
    public class MonomialVariablesOrderNormalizer : AstTransformationVisitorBase
    {
        private Stack<VariableExpression> _currentMonomialVars = new Stack<VariableExpression>();

        public override AstExpression VisitVariableExpression(VariableExpression expression)
        {
            _currentMonomialVars.Push(expression);

            if (_currentMonomialVars.Count == 1)
            {
                return expression;  
            }

            var right = _currentMonomialVars.Pop();
            var left = _currentMonomialVars.Pop();

            if (left.Name > right.Name)
            {
                var _ = right.Name;

                right.Name = left.Name;
                left.Name = _;
            }
            
            _currentMonomialVars.Push(left);
            _currentMonomialVars.Push(right);

            return expression;
        }
    }
}