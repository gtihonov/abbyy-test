﻿namespace Parser.Ast.Visitors
{
    public abstract class AstTransformationVisitorBase : IAstTransformationVisitor
    {
        public virtual AstExpression VisitEqualityExpression(EqualionExpression expression)
        {
            expression.Left = expression.Left.Accept(this);
            expression.Right = expression.Right.Accept(this);

            return expression;
        }

        public virtual AstExpression VisitNumberExpression(NumberExpression expression)
        {
            return expression;
        }

        public virtual AstExpression VisitVariableExpression(VariableExpression expression)
        {
            return expression;
        }

        public virtual AstExpression VisitAddExpression(AddExpression expression)
        {
            expression.Left = expression.Left.Accept(this);
            expression.Right = expression.Right.Accept(this);

            return expression;
        }

        public virtual AstExpression VisitMinusExpression(MinusExpression expression)
        {
            return expression.Accept(this);
        }

        public virtual AstExpression VisitPowExpression(PowExpression expression)
        {
            expression.Left = expression.Left.Accept(this);
            expression.Right = expression.Right.Accept(this);

            return expression;
        }

        public virtual AstExpression VisitMultiplyExpression(MultiplyExpression expression)
        {
            expression.Left = expression.Left.Accept(this);
            expression.Right = expression.Right.Accept(this);

            return expression;
        }
    }
}