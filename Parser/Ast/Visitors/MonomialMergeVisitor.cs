﻿using System.Collections.Generic;

namespace Parser.Ast.Visitors
{
    public class MonomialMergeVisitor : AstTransformationVisitorBase
    {
        private Dictionary<string, Queue<AstExpression>> _groupedMonimials = new Dictionary<string, Queue<AstExpression>>();
        
        
        private string _currentMonomialVariables;

        public override AstExpression VisitEqualityExpression(EqualionExpression expression)
        {
            base.VisitEqualityExpression(expression);

            foreach (var monomialGroup in _groupedMonimials)
            {
                if (monomialGroup.Value.Count == 1)
                {
                    continue;
                }

                var rootForAttach = (MultiplyExpression) monomialGroup.Value.Dequeue();
                while (monomialGroup.Value.Count != 0)
                {
                    var next = monomialGroup.Value.Dequeue();

                    decimal coef = 0;
                    if (next is MultiplyExpression multiplyExpression)
                    {
                        coef = ((NumberExpression) multiplyExpression.Left).Value;
                    }

                    if (next.Parent is AddExpression binaryExpression)
                    {
                        ((NumberExpression) rootForAttach.Left).Value += coef;
                        
                        if (binaryExpression.Left == next)
                        {
                            binaryExpression.Left = null;

                            if (binaryExpression.Parent is BinaryExpression parentBinaryExpression)
                            {
                                parentBinaryExpression.Right = binaryExpression.Right;
                                binaryExpression.Right.Parent = parentBinaryExpression;
                            }
                        }
                        
                        if (binaryExpression.Right == next)
                        {
                            binaryExpression.Right = null;

                            if (binaryExpression.Parent is BinaryExpression parentBinaryExpression)
                            {
                                parentBinaryExpression.Right = binaryExpression.Left;
                                binaryExpression.Left.Parent = parentBinaryExpression;
                            }
                        }
                    }
                }
                
                if (((NumberExpression) rootForAttach.Left).Value == 0)
                {
                    if (rootForAttach.Parent is AddExpression parentAddExpression)
                    {
                        if (parentAddExpression.Left == rootForAttach)
                        {
                            ((BinaryExpression) parentAddExpression.Parent).Right = parentAddExpression.Right;
                            parentAddExpression.Right.Parent = parentAddExpression.Parent.Parent;
                        }
                        
                        if (parentAddExpression.Right == rootForAttach)
                        {
                            ((BinaryExpression) parentAddExpression.Parent).Right = parentAddExpression.Left;
                            
                            parentAddExpression.Left.Parent = parentAddExpression.Parent.Parent;
                        }
                    }
                }
            }
            return expression;
        }

        public override AstExpression VisitAddExpression(AddExpression expression)
        {
            expression.Left = expression.Left.Accept(this);

            if (_currentMonomialVariables != string.Empty)
            {
                if (!_groupedMonimials.ContainsKey(_currentMonomialVariables))
                {
                    _groupedMonimials[_currentMonomialVariables] = new Queue<AstExpression>();
                }
                
                _groupedMonimials[_currentMonomialVariables].Enqueue(expression.Left);
            }

            _currentMonomialVariables = string.Empty;

            expression.Right = expression.Right.Accept(this);

            if (_currentMonomialVariables != string.Empty)
            {
                if (!_groupedMonimials.ContainsKey(_currentMonomialVariables))
                {
                    _groupedMonimials[_currentMonomialVariables] = new Queue<AstExpression>();
                }
                
                _groupedMonimials[_currentMonomialVariables].Enqueue(expression.Right);
            }

            _currentMonomialVariables = string.Empty;

            return expression;
        }

        public override AstExpression VisitVariableExpression(VariableExpression expression)
        {
            _currentMonomialVariables += expression.Name;

            return expression;
        }

        public override AstExpression VisitPowExpression(PowExpression expression)
        {
            expression.Left.Accept(this);
            _currentMonomialVariables += $"^{((NumberExpression) expression.Right).Value}";

            return expression;
        }
    }
}