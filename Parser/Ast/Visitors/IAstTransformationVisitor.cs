﻿namespace Parser.Ast.Visitors
{
    public interface IAstTransformationVisitor
    {
        public AstExpression VisitEqualityExpression(EqualionExpression expression);

        public AstExpression VisitNumberExpression(NumberExpression expression);
        public AstExpression VisitVariableExpression(VariableExpression expression);

        public AstExpression VisitAddExpression(AddExpression expression);

        public AstExpression VisitMinusExpression(MinusExpression expression);

        public AstExpression VisitPowExpression(PowExpression expression);

        public AstExpression VisitMultiplyExpression(MultiplyExpression expression);
    }
}