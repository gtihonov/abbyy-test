﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using CommandLine;
using Parser;
using Parser.Ast;
using Parser.Ast.Visitors;

namespace ConsoleApp1
{
    class Options
    {
        [Option('f', "file", Required = false,
            HelpText = "Input file to be processed.")]
        public string File { get; set; }
    }
    
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.CancelKeyPress += (sender, eventArgs) => Console.WriteLine("Bye");
            
            var tokenizer = new Tokenizer();
            if (args.Length >= 1)
            {
                Console.WriteLine("Executing in 'file' mode...");
                var fileName = args[0];
                
                if (!String.IsNullOrWhiteSpace(fileName))
                {
                    if (!File.Exists(fileName))
                    {
                        Console.WriteLine($"File {fileName} does not exists");
                    }


                    FileStream outFile = File.Create("out.txt");

                    await using var writeStream = new StreamWriter(outFile, Encoding.UTF8);
                
                    foreach (var input in await File.ReadAllLinesAsync(fileName))
                    {
                        var tokens = tokenizer.Tokenize(input);

                        var result = Process(tokens);
                        
                        if (result != null)
                        {
                            await writeStream.WriteLineAsync(result.ToString());
                        }
                        else
                        {
                            await writeStream.WriteLineAsync("Invalid input");
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Executing in 'interactive' mode...");

                while (true)
                {
                    Console.WriteLine("Please, enter input: ");
                    var input = Console.ReadLine();

                    var tokens = tokenizer.Tokenize(input);

                    var result = Process(tokens);

                    Console.WriteLine(result != null ? $"Result: {result}" : "Invalid input.");
                }
               
            }
        }

        private static AstExpression Process(IEnumerable<Token> tokens)
        {
            var astBuilder = new AstBuilder(tokens);
            var ast = astBuilder.ParseEquation();
                    
            var toLeftVisitor = new MoveAllMonomialsToLeftVisitor();
            var mergeVisitor = new MonomialMergeVisitor();

            return ast?
                .Accept(toLeftVisitor)
                .Accept(mergeVisitor);
        }
    }
}